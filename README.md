## Bloggit Project

The frontend of Bloggit. Created with [Angular](https://angular.io).

**To Run:**

- Install [Node.js](https://nodejs.org/) if it is not already installed
- Clone the repository
- In a terminal emulator opened in the root directory of the project, run

    ```bash
    npm install
    ```

    - After the dependencies finish installing, start the [backend](https://gitlab.com/hemard-csci-4990-01-2018/hemard-bloggit-be) by following the directions listed in its `README.md`
    - Next, run

    ```bash
    ng serve
    ```

- Finally, from a browser
    - Navigate to http://localhost:4200 