import { Injectable } from '@angular/core';
import { Bookmark } from './bookmark';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BookmarkService {
  private http: HttpClient;
  private url: string = 'http://localhost:8080/api/v1/bookmark';

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAllBookmarks(): Observable<Bookmark[]> {
    return this.http.get<Bookmark[]>(this.url);
  }

  getBookmarkById(bookmarkId: number): Observable<Bookmark> {
    return this.http.get<Bookmark>(`${this.url}/${bookmarkId}`);
  }

}