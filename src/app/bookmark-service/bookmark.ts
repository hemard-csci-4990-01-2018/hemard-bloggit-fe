export class Bookmark {
  bookmarkID: number;
  title: string;
  url: string;
  fullUrl: string;
  priority: number;
  createdBy: string;
  lastUpdated: number;
  visible: boolean;
}