import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { PostService } from '../post-service/post.service';
import { Post } from '../post-service/post';

@Component({
   selector: 'post-list',
   templateUrl: 'post-list.component.html',
   styleUrls: ['post-list.component.css']
})
export class PostListComponent {
    private error: HttpErrorResponse;
    private posts: Post[];

    constructor(postService: PostService) {
        postService.getAllPosts().subscribe(
            posts => this.posts = posts,
            error => this.error = error
        );
    }
}