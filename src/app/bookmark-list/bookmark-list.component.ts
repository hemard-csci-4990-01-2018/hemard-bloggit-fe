import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Bookmark } from '../bookmark-service/bookmark';
import { BookmarkService } from '../bookmark-service/bookmark.service';

@Component({
  selector: 'bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.css']
})
export class BookmarkListComponent { 
  private bookmarks: Bookmark[];
  private error: HttpErrorResponse;

  constructor(bookmarkService: BookmarkService) {
    bookmarkService.getAllBookmarks().subscribe(
      bookmarks => this.bookmarks = bookmarks,
      error => this.error = error
    );
  }
}
