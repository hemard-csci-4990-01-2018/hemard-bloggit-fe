import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { PostListComponent } from './post-list/post-list.component';
import { HeaderLinksComponent } from './header-links/header-links.component';
import { BookmarkListComponent } from './bookmark-list/bookmark-list.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { BookmarkDetailsComponent } from './bookmark-details/bookmark-details.component';
import { PostService } from './post-service/post.service';
import { BookmarkService } from './bookmark-service/bookmark.service';
import { PostFormComponent } from './post-form/post-form.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        PostListComponent,
        HeaderLinksComponent,
        BookmarkListComponent,
        PostDetailsComponent,
        BookmarkDetailsComponent,
        PostFormComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'posts',
                pathMatch: 'full'
            },
            {
                path: 'posts',
                component: PostListComponent
            },
            {
                path: 'posts/create',
                component: PostFormComponent
            },
            {
                path: 'posts/:postId',
                component: PostDetailsComponent
            },
            {
                path: 'posts/:postId/update',
                component: PostFormComponent
            },
            {
                path: 'bookmarks',
                component: BookmarkListComponent
            },
            {
                path: 'bookmarks/:bookmarkId',
                component: BookmarkDetailsComponent
            }
        ]),
        HttpClientModule,
        FormsModule
    ],
    providers: [
        PostService,
        BookmarkService
    ],
    bootstrap: [AppComponent]
 })
export class AppModule { }